# Quick Media Edit

## TL:DR
This module adds a 'destination' parameter to the url in order to guide the user back to the initial path after editing a media image.

## Problem
If a user wants to edit a media image, such as it's focal point, he:she must first find the image in the media library. There's no way to edit it directly from the node form.
However, one can change the view mode 'media library' to 'linked to content'. This link leads directly to the edit form of the media image. But after saving the image the user is redirected to the media library instead of back to the node edit form.

## Solution
This module checks whether the user has the permission to edit the referenced image and if so, adds a destination parameter to the url that redirects the user back to the node form after saving the image.

##Caveats
The module currently only works for images. Any help in expanding it's scope is appreciated and you're welcome to open issues and provide patches.

## Installation / Configuration
- Install and enable the module
- Go to /admin/structure/media/manage/image/display/media_library and configure the image to be linked with 'content'.
